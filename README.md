# mastodon-alt-bot (alpha)

To build the software, install a recent version of the [Go compiler](https://go.dev).
Once done, you can simply invoke a `go build` and it's working, yay.

To use it, create an application on the Mastodon web interface, e.g. https://queer.group/settings/applications
and add a new application with the following scopes:

- `read:follows`
- `read:statuses`
- `write:follows`
- `write:statuses`
- `follow`

(todo: check whether all the mentioned scopes are actually required)

Afterwards, use the credentials and start the bot like this:

```shell
$ go run . -client-id [your_client_id] -client-secret [your_client_secret] -access-token [your_access_token]
```

## Contributing

Merge Requests and Issues are welcomed <3

## License

The bot is licensed under the OSL-3.0.
