package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/mattn/go-mastodon"
)

const userAgent = "Mastodon alternate text reminder bot by @jasmin@queer.group"

var (
	fs           = flag.NewFlagSet("mastodon-alt-text-reminder", flag.ExitOnError)
	server       = fs.String("server", "https://queer.group", "The URL of the Mastodon server")
	clientID     = fs.String("client-id", "", "The client ID for the application")
	clientSecret = fs.String("client-secret", "", "The client secret for the application")
	accessToken  = fs.String("access-token", "", "The access token for the bot account")
)

func main() {
	if err := fs.Parse(os.Args[1:]); err != nil {
		log.Fatalf("parsing arguments failed: %v", err)
	}

	if *server == "" {
		log.Fatalf("The -server flag is empty, please provide a value.")
	}
	if *clientID == "" {
		log.Fatalf("The -client-id flag is empty, please provide a value.")
	}
	if *clientSecret == "" {
		log.Fatalf("The -client-secret flag is empty, please provide a value.")
	}
	if *accessToken == "" {
		log.Fatalf("The -access-token flag is empty, please provide a value.")
	}

	c := mastodon.NewClient(&mastodon.Config{
		Server:       *server,
		ClientID:     *clientID,
		ClientSecret: *clientSecret,
		AccessToken:  *accessToken,
	})
	c.UserAgent = userAgent

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	evts, err := c.StreamingUser(ctx)
	if err != nil {
		log.Printf("streaming users failed: %v", err)
		return
	}

	go func() {
		for range time.Tick(10 * time.Minute) {
			accts, err := c.GetFollowRequests(ctx, &mastodon.Pagination{Limit: 100})
			if err != nil {
				log.Printf("errors during follow request fetch: %v", err)
			}

			for _, acct := range accts {
				if err := c.FollowRequestAuthorize(ctx, acct.ID); err != nil {
					log.Printf("could not accept follow request for user %s: %v", acct.Acct, err)
				}

				if _, err := c.AccountFollow(ctx, acct.ID); err != nil {
					log.Printf("could not follow user %s: %v", acct.Acct, err)
				}
			}
		}
	}()

	for e := range evts {
		switch event := e.(type) {
		case *mastodon.UpdateEvent:
			if _, err := handleStatus(ctx, c, event.Status); err != nil {
				log.Printf("handling status %s failed: %v", event.Status.ID, err)
			}
		case *mastodon.ErrorEvent:
			// TODO: This is damn wrong, maybe we should build the API wrapper ourselves.
			if strings.Contains(event.Error(), "401 Unauthorized") {
				log.Printf("authentication error, aborting: %s", event.Error())
				return
			}

			log.Printf("streaming error: %s", event.Error())
		}
	}
}

type statusHandler interface {
	PostStatus(ctx context.Context, toot *mastodon.Toot) (*mastodon.Status, error)
}

func handleStatus(ctx context.Context, client statusHandler, status *mastodon.Status) (handled bool, err error) {
	boostStatus := checkForMissingMediaDescriptions(status.Reblog)
	tootStatus := checkForMissingMediaDescriptions(status)

	if tootStatus.HasAllDescriptions() && boostStatus.HasAllDescriptions() {
		return false, nil
	}

	toot := buildReminderToot(status)
	if _, err := client.PostStatus(ctx, toot); err != nil {
		return false, fmt.Errorf("replying to user failed: %v", err)
	}

	return true, nil
}

type tootTemplate map[string]map[string]string

func (tt tootTemplate) GetForStatus(status *mastodon.Status) string {
	if status == nil {
		return ""
	}
	if status.Reblog != nil {
		status = status.Reblog
	}
	if len(status.MediaAttachments) == 0 {
		return ""
	}

	attachmentType := status.MediaAttachments[0].Type
	for k, v := range tt[attachmentType] {
		if k == status.Language {
			return v
		}
	}

	// If we haven't returned yet, take the default language.
	return tt[attachmentType]["en"]
}

var (
	tootTemplates = tootTemplate{
		"image": {
			"de": "Hi @%s! Weil du mich um Vorschläge gebeten hast: Dem Bild fehlt eine Beschreibung. Du könntest den Toot noch löschen und mit Caption neu posten oder bearbeiten! Danke!\n\nTipps für Bildbeschreibungen gibt's übrigens hier: https://www.dbsv.org/bildbeschreibung-4-regeln.html",
			"en": "Hi @%s! Because you asked for reminders about missing image descriptions: one or more images are missing a description. But don't worry, you can still delete or edit your toot! Thanks!",
		},
		"video": {
			"de": "Hi @%s! Du hattest mich um Hinweise gebeten. Dem Video fehlt leider eine Beschreibung! Du kannst den Toot noch löschen und das Video mit einer Beschreibung versehen. Damit würdest du dazu beitragen, das Fediverse für uns alle inklusiver zu gestalten!",
			"en": "Hi @%s! You've asked me for reminders and unfortunately, the video doesn't have a description. But you can still delete or edit the toot and add the missing description. Thanks in advance!",
		},
		"audio": {
			"de": "Hi @%s! Weil du mich um Vorschläge gebeten hast: auch Audio-Aufnahmen und Musik brauchen eine Beschreibung! Es wäre großartig von dir, wenn du den Toot löschen und mit einer Beschreibung versehen könntest. Danke!",
			"en": "Hi @%s! You've asked me for reminders and unfortunately, the audio doesn't have a description. But you can still delete or edit the toot and add the missing description. Thanks in advance!",
		},
	}
	boostTemplates = tootTemplate{
		"image": {
			"de": "Hi @%s! Dein Boost enthält leider keine Bildbeschreibung(en), was sehr schade ist. Du kannst den*die Autor*in darauf hinweisen, damit das Fediverse für uns alle inklusiver wird.",
			"en": "Hi @%s! Your boost does not contain any image description(s), which is a pity. You can point this out to the author to make Fediverse more inclusive for all of us.",
		},
		"video": {
			"de": "Hi @%s! Dein Boost enthält leider keine Beschreibung für das Video, was sehr schade ist. Du kannst den*die Autor*in darauf hinweisen, damit das Fediverse für uns alle inklusiver wird.",
			"en": "Hi @%s! Your boost does not contain any video description(s), which is a pity. You can point this out to the author to make Fediverse more inclusive for all of us.",
		},
		"audio": {
			"de": "Hi @%s! Dein Boost enthält leider keine Beschreibung für die Audio-Aufnahme/Musik, was sehr schade ist. Du kannst den*die Autor*in darauf hinweisen, damit das Fediverse für uns alle inklusiver wird.",
			"en": "Hi @%s! Your boost does not contain any audio description(s), which is a pity. You can point this out to the author to make Fediverse more inclusive for all of us.",
		},
	}
)

func buildReminderToot(status *mastodon.Status) *mastodon.Toot {
	if status.Reblog != nil {
		return &mastodon.Toot{
			Status:      fmt.Sprintf(boostTemplates.GetForStatus(status), status.Account.Acct),
			InReplyToID: status.ID,
			Visibility:  "direct",
			Language:    status.Reblog.Language,
		}
	}

	return &mastodon.Toot{
		Status:      fmt.Sprintf(tootTemplates.GetForStatus(status), status.Account.Acct),
		InReplyToID: status.ID,
		Visibility:  "direct",
		Language:    status.Language,
	}
}

type mediaDescriptionResult struct {
	Image bool
	Video bool
	Audio bool
}

func (m mediaDescriptionResult) HasMissingDescriptions() bool {
	return !m.HasAllDescriptions()
}

func (m mediaDescriptionResult) HasAllDescriptions() bool {
	return m.Image && m.Video && m.Audio
}

func checkForMissingMediaDescriptions(status *mastodon.Status) mediaDescriptionResult {
	result := mediaDescriptionResult{true, true, true}
	if status == nil {
		return result
	}

	for _, a := range status.MediaAttachments {
		if a.Description != "" {
			continue
		}

		switch a.Type {
		case "image":
			result.Image = false
		case "video":
			result.Video = false
		case "audio":
			result.Audio = false
		}
	}

	return result
}
