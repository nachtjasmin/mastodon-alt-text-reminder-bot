package main

import (
	"context"
	"testing"

	"github.com/mattn/go-mastodon"
)

func Test_hasAltTextOnImages(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name   string
		status mastodon.Status
		want   bool
	}{
		{
			name:   "just text",
			status: mastodon.Status{Content: "just text"},
			want:   true,
		},
		{
			name: "single image without description",
			status: mastodon.Status{
				MediaAttachments: []mastodon.Attachment{{
					Type:        "image",
					Description: "",
				}},
			},
			want: false,
		},
		{
			name: "multiple images, one without description",
			status: mastodon.Status{
				MediaAttachments: []mastodon.Attachment{{
					Type:        "image",
					Description: "has alt text",
				}, {
					Type:        "image",
					Description: "",
				}},
			},
			want: false,
		},
		{
			name: "multiple images, all with description",
			status: mastodon.Status{
				MediaAttachments: []mastodon.Attachment{{
					Type:        "image",
					Description: "has alt text",
				}, {
					Type:        "image",
					Description: "also has alt text",
				}},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := checkForMissingMediaDescriptions(&tt.status)
			if got.Image != tt.want {
				t.Errorf("hasAltTextOnImages() = %v, want %v", got, tt.want)
			}
		})
	}
}

type mockClient struct{}

func (m mockClient) PostStatus(context.Context, *mastodon.Toot) (*mastodon.Status, error) {
	return &mastodon.Status{}, nil
}

func TestHandleStatus(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name   string
		status mastodon.Status
		want   bool
	}{
		{
			name:   "regular status without any images",
			want:   false,
			status: mastodon.Status{Content: "test"},
		},
		{
			name: "regular status with one missing image description",
			want: true,
			status: mastodon.Status{
				Content: "messing around",
				MediaAttachments: []mastodon.Attachment{{
					Type:        "image",
					Description: "",
				}},
			},
		},
		{
			name: "boosted status with one missing image description",
			want: true,
			status: mastodon.Status{
				Content: "",
				Reblog: &mastodon.Status{
					MediaAttachments: []mastodon.Attachment{{
						Type:        "image",
						Description: "",
					}},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _ := handleStatus(context.Background(), mockClient{}, &tt.status)
			if got != tt.want {
				t.Errorf("got %t, want %t", got, tt.want)
			}
		})
	}
}
